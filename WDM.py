import stanza
from gensim.models import Word2Vec
from gensim.corpora import Dictionary
import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt

# Download and initialize Latin neural pipeline
stanza.download('la')
nlp = stanza.Pipeline(lang='la')

# List of files to be processed
files = [
    "DeScientiis.txt",
    "DeUnitateetUno.txt",
    "DeProcessioneMundi.txt",
    "DeImmortalitateAnimae.txt",
    "DeDivisionePhilosophiae.txt",
    "DeAnima.txt", "MetaphysicsIbnSina.txt", "MaqasidafalasifaGazali.txt",
    "deDiffinitionibusIsraeli.txt",
    "DeIntellectuFarabi.txt", "DeIntellectuKindi.txt",
    "FonsvitaeIbnGabirol.txt"
]

# Preprocess the texts
documents = []
for file in files:
    with open(file, 'r') as f:
        text = f.read()
        # Process the text to tokenize and lemmatize
        doc = nlp(text)
        # Append all lemmatized words from the document
        documents.append([word.lemma for sent in doc.sentences for word in sent.words])

# Train a Word2Vec model
# Note: You might need a larger corpus for a more accurate model
model = Word2Vec(documents, vector_size=100, window=5, min_count=1, workers=4)

# Calculate WMD between each pair of documents
for i, document in enumerate(documents):
    for j, other_document in enumerate(documents):
        if i != j:
            wmd = model.wv.wmdistance(document, other_document)
            print(f'WMD between "{files[i]}" and "{files[j]}": {wmd}')

# List of concepts with their different forms
concepts = [
    ["materia", "materiam", "materiae"],  # replace with the actual forms of "materia"
    ["forma", "formam", "formae"],  # replace with the actual forms of "forma"
    ["voluntas", "uoluntas", "voluntatem", "uoluntatem"]  # replace with the actual forms of "voluntas"
]

# Initialize a dictionary to store the WMDs
wmd_dict = {concept[0]: [] for concept in concepts}

# Calculate WMD between each concept and each document
for concept_list in concepts:
    for i, document in enumerate(documents):
        wmd = model.wv.wmdistance(concept_list, document)
        wmd_dict[concept_list[0]].append(wmd)
        print(f'WMD between "{concept_list[0]}" and "{files[i]}": {wmd}')

# Convert the dictionary to a DataFrame
wmd_df = pd.DataFrame(wmd_dict, index=files)

# Create a heatmap
plt.figure(figsize=(10, 8))
sns.heatmap(wmd_df, cmap="YlGnBu")
plt.title('Word Mover\'s Distance between Concepts and Documents')
plt.xlabel('Concepts')
plt.ylabel('Documents')
plt.show()