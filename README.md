
# Translating Thought: A Digital Inquiry into Gundissalinus's Interpretive Influence on Ibn Gabirol's Yanbu' al-Haya

This repository contains the dataset and code used in the article "Translating Thought: A Digital Inquiry into Gundissalinus's Interpretive Influence on Ibn Gabirol's Yanbu' al-Haya." The purpose of this repository is to provide the necessary materials for reproducing the analyses and visualizations presented in the article.

## Contents
- visuals.zip: This archive contains the figures and tables used in the article.
- DATA Corpus TXT.zip: This archive contains the text files of Gundissalinus' original works and translations.
- WDM.py: Python script for performing Weighted Document Model analysis.
- LSA.py: Python script for performing Latent Semantic Analysis.
- CosineSimilarity.py: Python script for calculating cosine similarity between documents.
