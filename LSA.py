# Given the computational constraints, we'll attempt an alternative method for visualization.
# We'll use PCA for dimensionality reduction, which is less computationally intensive than t-SNE.
import gensim
from sklearn.decomposition import PCA

# Applying PCA to reduce the data to 2 dimensions for visualization purposes
pca = PCA(n_components=2)
dtm_lsa_pca = pca.fit_transform(dtm_lsa_reduced)

# Plotting with PCA reduced dimensions
fig, ax = plt.subplots(figsize=(10, 6))

# Plot each category with different colors
for category, color in category_colors.items():
    indices = [i for i, doc in enumerate(documents) if doc in categories[category]]
    ax.scatter(dtm_lsa_pca[indices, 0], dtm_lsa_pca[indices, 1], c=color, label=category, s=100)

# Annotating document names
for i, txt in enumerate(documents):
    ax.annotate(txt, (dtm_lsa_pca[i, 0]+0.01, dtm_lsa_pca[i, 1]+0.01), fontsize=9)

plt.title('PCA Visualization of Documents by LSA Topics')
plt.xlabel('PCA Component 1')
plt.ylabel('PCA Component 2')
plt.legend(title='Document Category')
plt.grid(True)
plt.show()
# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.


# See PyCharm help at https://www.jetbrains.com/help/pycharm/
fig, ax = plt.subplots(figsize=(12, 8))

# Plot each category with different colors
for category, docs in categories.items():
    idx = [documents.index(doc) for doc in docs if doc in documents]
    pca_x = dtm_lsa_pca[idx, 0]
    pca_y = dtm_lsa_pca[idx, 1]
    ax.scatter(pca_x, pca_y, label=category, s=100)

# Annotating document names
for i, doc in enumerate(documents):
    ax.annotate(doc, (dtm_lsa_pca[i, 0], dtm_lsa_pca[i, 1]))

ax.set_title('Visualization of Documents by LSA Topics Using PCA')
ax.set_xlabel('PCA Component 1')
ax.set_ylabel('PCA Component 2')
ax.legend(title='Document Category')
plt.show()
