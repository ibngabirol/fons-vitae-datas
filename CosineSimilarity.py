import os
import string
import numpy as np
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.metrics.pairwise import cosine_similarity
from gensim.utils import simple_preprocess
import matplotlib.pyplot as plt

# File paths
file_paths = {
    'translations': [
        '/mnt/data/MaqasidafalasifaGazali.txt',
        '/mnt/data/deDiffinitionibusIsraeli.txt',
        '/mnt/data/MetaphysicsIbnSina.txt',
        '/mnt/data/DeIntellectuKindi.txt',
        '/mnt/data/DeIntellectuFarabi.txt'
    ],
    'original_works': [
        '/mnt/data/DeScientiis.txt',
        '/mnt/data/DeUnitateetUno.txt',
        '/mnt/data/DeProcessioneMundi.txt',
        '/mnt/data/DeImmortalitateAnimae.txt',
        '/mnt/data/DeDivisionePhilosophiae.txt',
        '/mnt/data/DeAnima.txt'
    ],
    'fons_vitae': [
        '/mnt/data/FonsvitaeIbnGabirol.txt'
    ]
}


def read_file(file_path):
    with open(file_path, 'r', encoding='utf-8') as file:
        return file.read()


# Reading file contents
texts = {}
for category, paths in file_paths.items():
    texts[category] = [read_file(path) for path in paths]


# Preprocessing texts using Gensim's simple_preprocess
def preprocess(text):
    return " ".join(simple_preprocess(text, deacc=True))  # deacc=True removes punctuations


preprocessed_texts = {category: [preprocess(text) for text in texts_list] for category, texts_list in texts.items()}

# Vectorizing texts
vectorizer = CountVectorizer()
tfidf_transformer = TfidfTransformer()

# Concatenating all texts for vocabulary fitting
all_texts = sum([texts for texts in preprocessed_texts.values()], [])
count_matrix = vectorizer.fit_transform(all_texts)
tfidf_matrix = tfidf_transformer.fit_transform(count_matrix)

# Extracting feature matrix for each category
features = {
    category: tfidf_transformer.transform(vectorizer.transform([preprocess(" ".join(texts_list))]))
    for category, texts_list in texts.items()
}

# Calculating cosine similarity between Fons Vitae and other categories
cosine_similarities = {
    'Fons Vitae vs Translations': cosine_similarity(features['fons_vitae'], features['translations'])[0, 0],
    'Fons Vitae vs Original Works': cosine_similarity(features['fons_vitae'], features['original_works'])[0, 0]
}

print(cosine_similarities)
